# AlphaBlend
AlphaBlend 是 Windows 和 Flutter 都在使用的 alpha 通道混合的算法，在 VS 的 Windows 应用开发里有接口。但考虑到使用平台可能不局限于 Windows，需要单独实现 AlphaBlend 函数。  

**计算公式**:假设一幅图象是A，另一幅透明的图象是B，那么透过B去看A，看上去的图象C就是B和A的混合图象，设B图象的透明度为alpha(取值为0-255，1为完全透明，255为完全不透明)。Alpha混合公式如下：  
A(C)=A(B) + (A(A) \* (255 - A(B)))  
R(C)=(255-alpha)\*R(B) + alpha\*R(A)  
G(C)=(255-alpha)\*G(B) + alpha\*G(A)  
B(C)=(255-alpha)\*B(B) + alpha\*B(A)  

# 简易 Alpha 混合算法
这个方法计算较快，但没有给出 Alpha 值的计算方法。经过分析，应该只适用于下层为不透明的情况。  
r1,g1,b1是上层的颜色值；r2,g2,b2是下层颜色值.若Alpha=上层透明度，则:  
**当 Alpha=50% 时:**  
r = r1/2 + r2/2;  
g = g1/2 + g2/2;  
b = b1/2 + b2/2;  
**当 Alpha<50% 时:**  
r = r1 - r1/ALPHA + r2/ALPHA;  
g = g1 - g1/ALPHA + g2/ALPHA;  
b = b1 - b1/ALPHA + b2/ALPHA;  
**当 Alpha>50% 时:**  
r = r1/ALPHA + r2 - r2/ALPHA;  
g = g1/ALPHA + g2 - g2/ALPHA;  
b = b1/ALPHA + b2 - b2/ALPHA;  

# 透明度混合算法三
R1、G1、B1、Alpha1 为前景颜色值，R2、G2、B2、Alpha2 为背景颜色值，则：  
Alpha = 255 - (255 - Alpha1) * ( 255 - Alpha2)  
R = (R1 * Alpha1 + R2 * Alpha2 * (255-Alpha1))/Alpha  
G = (G1 * Alpha1 + G2 * Alpha2 * (255-Alpha1))/Alpha  
B = (B1 * Alpha1 + B2 * Alpha2 * (255-Alpha1))/Alpha  
