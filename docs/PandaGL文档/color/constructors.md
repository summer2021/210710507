
## Color
**变量**  
int: value  表示颜色  
**说明**  
用32位表示颜色
24-31 位表示透明度。  
16-23 位表示红色。  
8-15 位表示绿色。  
0-7  位表示蓝色。  
