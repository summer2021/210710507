# 图形类
---------
## Graph
Graph 是所有图形类的父类  

**变量**  
　*pos*: m_pos  　图形的锚点所在的像素位置（对于直线、矩形来说是图像的的左上角；对于椭圆，是圆心)

## line: Graph
普通线段类

**变量**  
　int: m_line_type  　X 方向线段或 Y 方向线段  
　double: m_length  　线段长度  
　double: m_width  　线段宽度  
　*color*: m_color  　线段颜色  
　
## Oval: Graph
椭圆类

**变量**  
　double: m_horizontal_length  　X 方向半轴的长度  
　double: m_vertical_length  　Y 方向半轴的长度  
　*color*: m_inner_color  　椭圆内部颜色  
　*color*: m_frame_color  　椭圆边框颜色  
　int: m_frame_type  　边框样式（向外，居中，向内）  

**函数**
  　getPos(double begin_pos, double end_pos)  　pos 数值从0到1，X 轴正方向为0，逆时针旋转。用极坐标计算。
　
## Circle: Oval
标准圆类，继承于 Oval 类。

**变量**  
　double: m_radius  　圆的半径  
　
**函数**
  　getPos(double begin_pos, double end_pos)  重载 Oval 中的getPos函数。
## Rect: Graph
普通矩形类

**变量**  
　double: m_horizontal_length  　矩形 X 方向长度  
　double: m_vertical_length  　矩形 Y 方向长度  
　double: m_frame_width  　矩形边框宽度。用于方便获取，绘制时使用line内数据为准，更改时同步更改  
　*color*: m_inner_color  　矩形内部颜色  
　*color*: m_frame_color  　矩形边框颜色。用于方便获取，绘制时使用line内数据为准，更改时同步更改  
　*line*: m_top_line  　矩形上方边  
　*line*: m_bottom_line  　类上  
　*line*: m_left_line  　类上  
　*line*: m_right_line  　类上  
　int: m_frame_type  　边框样式（向外，居中，向内）  

## Rounded_rect: Rect
圆角矩形类，继承于 Rect 类。

**变量**  
　*Oval*: m_top_left_round  　圆角矩形左上角曲线  
　*Oval*: m_top_right_round  　类上  
　*Oval*: m_bottom_left_round  　类上  
　*Oval*: m_bottom_right_round  　类上  
　
　
