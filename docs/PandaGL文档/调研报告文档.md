# 与OpenGL对比
**OpenGL绘制逻辑：**  
1. 创建窗口。设置显示模式，窗口位置与大小，窗口名称等。
![avatar](pictures/OpenGL_CreateWindow.png)
2. 初始化函数。如视图模式，变换模式等的设置。通过glMatrixMode(), glLoadIdentity(), gluPerspective()等函数实现。
3. 显示绘制函数。把物体绘制出。display函数，在主程序中通过glutDisplayFun(display)调用。
4. 窗口重定型函数。当改变窗口纵横比，物体因此也会发生变化，这个函数维护形状改变时绘制的正确性。  

在OpenGL中，图形绘制分为点、线、三角形、多边形等多种。在OpenGL中，线段可以进行斜线的绘制。对于圆角矩形中的圆弧曲线，在OpenGL中可以通过画连续曲线方法，将圆弧分为n段绘制。
# 与AGG对比
AGG是一个高效的、高质量的、开源的矢量图形库，反锯齿效果超好。从易用性的角度来说AGG比较困难，因为官方只给出了C++源代码,没有组件库和封装好的包；但AGG较高效，跨平台效果好。  
AGG图形显示原理如下：
1.	Vertex Source 顶点源，里面存放了一堆2D顶点以及对应的命令，如"MoveTo"、"LineTo"等。
2.	Coordinate conversion pipeline 坐标转换管道，它可以变换Vertex Source中的顶点，比如矩阵变换，轮廓提取，转换为虚线等。
3.	Scanline Rasterizer 把顶点数据（矢量数据）转换成一组水平扫描线，扫描线由一组线段(Span)组成，线段(Span)包含了起始位置、长度和覆盖率（可以理解为透明度）信息。AGG的抗锯齿（Anti-Aliasing）功能也是在这时引入的。
4.	Renderers 渲染器，渲染扫描线(Scanline)中的线段(Span)，最简单的就是为Span提供单一颜色，复杂的有多种颜色(如渐变)、使用图像数据、Pattern等。
5.	Rendering Buffer 用于存放像素点阵数据的内存块，这里是最终形成的图像数据。  
<img src="pictures/AGG_display.png" width="250">

AGG图形显示原理(https://www.cnblogs.com/CoolJie/archive/2011/04/27/2030037.html)
# 与QT对比
QT是一个跨平台的 C++ 开发库，主要用来开发图形用户界面，包含还比如多线程、访问数据库、图像处理等很多其他功能。本项目只考虑与其GUI库（QPainter）对比。  

QPainter类在窗口和其他绘制设备上执行低级绘制。提供高度优化的功能来完成大多数图形用户界面程序所需的工作。它能画出从简单线条到复杂形状如饼图和弦等一切图形。它还可以绘制对齐的文本和像素图。通常，它绘制一个“自然”坐标系，但它也可以进行视图和世界变换。  

QPainter的常见用法是在窗口的绘制事件中：构造和自定义（例如设置笔或画笔）绘制器。然后进行绘制，最后销毁QPainter对象。QPainter中同样提供了绘制大多数基本体的函数。如drawPoint()、drawPoints()、drawLine()、drawRect()、drawRoundedRect()、drawEllipse()等。
在QT中绘制圆角矩形可以通过两种方法：
1. 使用setStyleSheet方法，能够设置四个角的半径。
2. 在QWidget中进行测试绘制圆角矩形函数drawRoundRect，drawRoundedRect如下。

<img src="pictures/QT_RoundRect.png">

QT绘制圆角矩形函数（https://blog.csdn.net/qq21497936/article/details/105506028）  
函数参数如下，有矩形的基本位置信息，以及x、y方向的半径长度，以及矩形大小的显示模式。
<img src="pictures/QT_functionParameter.png"> 
