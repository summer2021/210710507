# Cairo

Cairo 是GTK+采用的底层图形库，是一款开源的2d矢量图形库，负责构建图形界面。矢量图元是用数学方程式进行精确描述的。  
统一了图形库的操作方式，同时支持PS、PDF、SVG、PNG/JPEG等图像格式的输出，方便页面的再次利用，在glitz的支持下支持部分3D效果。  
Cairo 能够做各种复杂的点线图案绘制、填充、文字渲染、图像变换、剪切、层混合等等操作。  
Cairo 支持多后端的设计。

## 基本概念

名词介绍：  
Destination：相当于画布  
Source：相当于涂料  
Mask：相当于蒙版，控制涂料作用在画布的哪些地方  
Path：由路径相关的动词控制，最终作用在蒙版上  
Context：它记录着一个源，一个目标，一个蒙板。同时它还记录着像线宽等辅助变量。最重要的是上下文会记录路径，路径最终会被绘制动词转换成蒙板。  

Cairo 绘图模型依赖于一个三层模型。

任何绘图过程都分三步进行：

1. 首先创建一个蒙版，其中包括一个或多个矢量基元或形式，即圆形、正方形、字体、贝塞尔曲线等。
2. 然后必须定义源，它可以是颜色、颜色渐变、位图或一些矢量图形，并且在上述定义的掩码的帮助下，从该源的绘制部分进行模切。
3. 最后使用 Cairo_stroke() 或 Cairo_fill() 将结果传输到 Destination 或 Surface ，为后端提供输入。

Cairo 在2个层之间使用不同的混合算法的效果，共有16种。比如变量、变暗、正片叠底等。列出常用的几种：  
其中：aR为结果的alpha值，xR为结果的颜色值。
1. CAIRO_OPERATOR_OVER  
表示两个半透明图片叠在一起。  
aR = aA + aB·(1−aA)  
xR = (xaA + xaB·(1−aA))/aR

2. CAIRO_OPERATOR_DEST_OUT  
第二个对象用于降低重叠区域中第一个对象的可见性。第二个对象本身没有被绘制。  
aR = (1−aA)·aB  
xR = xB

3. CAIRO_OPERATOR_ADD  
颜色叠加模式。  
aR = min(1, aA+aB)  
xR = (xaA + xaB)/aR



### 数据结构


#### _cairo_line
由两个 cairo_point_t 组成，分别表示起点与终点

#### _cairo_rectangle & _cairo_rectangle_int
内含四个变量：矩形左上角 x,y 矩形宽高 width, height。

#### 颜色
Cairo 颜色有不同的表示方式，如 ARGB_32, RGB16_565 等。在使用时，直接向绘制函数传入rgba值。
```c
cairo_set_source_rgb (cr, r, g, b, a)
```  

### 函数

```c
/*
初始化 cairo 上下文
*/
{
cairo_surface_t *surface;
cairo_t *cr;
surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 120, 120);
cr = cairo_create (surface);
}

/*
设置路径Path的起点
*/
cairo_move_to (cairo_t cr, double x, double y);

/*
从Path终点扩展路径到该点
*/
cairo_line_to (cairo_t cr, double x, double y);


/*
一个虚拟画笔沿着特定的路径（path）描边. 画笔所过之处使得源（Source）
通过一个空心或实心的路径线蒙板（mask）传输到目标,
路径线取决于画笔的 线宽（line width）, 表现样式（dash style）, 和 线头（line caps）.
*/
Cairo_stroke(cairo_t cr)

/*
填充路径中间部分
*/
Cairo_fill(cairo_t cr)
```

## 示例

### 绘制线段
```c
cairo_t *cr;
//生成环境
cr = gdk_cairo_create (widget->window );
 //设置线段颜色和粗细   
cairo_set_source_rgb (cr,  0,  0,  0 );
cairo_set_line_width  (cr,  0.5 );
//cairo_move_to和cairo_line_to在cr中定义了绘图路径
cairo_line_to (cr, 0.5, 0.375);
cairo_rel_line_to (cr, 0.25, -0.125);
//cairo_stroke将cr中的路径绘制出来
cairo_stroke (cr );
cairo_destroy (cr );
```

### 绘制矩形

```c
cairo_t *cr;
cr = gdk_cairo_create(widget->window);
cairo_set_source_rgb(cr,0,0,0);
cairo_set_line_width(cr,1);
//绘制一个矩形，起点是20 20，长120，宽80
cairo_rectangle(cr,20,20,120,80);
//绘制边线，且保留路径
cairo_stroke_preserve(cr);
cairo_set_source_rgb(cr,0,0,1);
//填充，且清除路径
cairo_fill(cr);
```


### 透明度混合
```c
///被调用的绘制函数。绘制两个正方向在不同混色模式下的效果
draw  (cairo_t * cr, gint x, gint w, gint h, cairo_operator_t op )
{
        //初始化
        cairo_t *first_cr, *second_cr;
        cairo_surface_t *first, *second;

        first = cairo_surface_create_similar  (cairo_get_target  (cr ),
                                              CAIRO_CONTENT_COLOR_ALPHA,
                                              w, h );

        second = cairo_surface_create_similar  (cairo_get_target  (cr ),
                                               CAIRO_CONTENT_COLOR_ALPHA,
                                               w, h );

        //设置矩形样式
        first_cr = cairo_create  (first );
        cairo_set_source_rgb  (first_cr,  0,  0,  0.4 );
        cairo_rectangle  (first_cr, x,  20,  50,  50 );
        cairo_fill  (first_cr );

        second_cr = cairo_create  (second );
        cairo_set_source_rgb  (second_cr,  0.5,  0.5,  0 );
        cairo_rectangle  (second_cr, x +  10,  40,  50,  50 );
        cairo_fill  (second_cr );

        //设置叠加样式
        cairo_set_operator  (first_cr, op );
        cairo_set_source_surface  (first_cr, second,  0,  0 );
        cairo_paint  (first_cr );

        cairo_set_source_surface  (cr, first,  0,  0 );
        cairo_paint  (cr );

        cairo_surface_destroy  (first );
        cairo_surface_destroy  (second );

        cairo_destroy  (first_cr );
        cairo_destroy  (second_cr );
}

on_expose_event  (GtkWidget * widget,
                 GdkEventExpose * event, gpointer data )
{
        cairo_t *cr;
        gint w, h;
        gint x, y;


        //叠加模式
        cairo_operator_t oper [ ] =  {
                CAIRO_OPERATOR_DEST_OVER,
                CAIRO_OPERATOR_DEST_IN,
                CAIRO_OPERATOR_OUT,
                CAIRO_OPERATOR_ADD,
                CAIRO_OPERATOR_ATOP,
                CAIRO_OPERATOR_DEST_ATOP,
         };

        gtk_window_get_size  (GTK_WINDOW  (widget ), &w, &h );

        cr = gdk_cairo_create  (widget->window );

        //测试6种叠加模式
        gint i;
         for  (x =  20, y =  20, i =  0; i <  6; x +=  80, i++ )  {
                draw  (cr, x, w, h, oper [i ] );
         }

        cairo_destroy  (cr );

         return  FALSE;
}

```

### 直接操作像素数据

Cairo 是矢量图形库，未找到 Cairo 直接操作像素数据的接口。
