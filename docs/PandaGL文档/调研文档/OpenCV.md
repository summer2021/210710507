# OpenCV

OpenCV 发行目标是作为计算机视觉和机器学习软件库。它轻量级而且高效，具有跨平台的优点，且提供了多种语言的接口。OpenCV 的重点在于为计算机视觉提供功能支撑，比如图片降噪等。它也包含图形库常用的 2D 绘制功能。

## 基本概念

虽然 OpenCV 中具有 Rect 对象，但较少使用到。绘制时一般直接通过普通函数 line rectangle 等直接进行绘制。

**绘制流程：**
1. 创建 Mat 对象(像素矩阵)
2. 调用绘制函数。在函数中设置位置、颜色、填充等信息。
3. 通过 imwrite 输出到文件，或者 imshow 输出到屏幕。


**外部库:**  
OpenCV 同样调用了 libpng, libjpeg, libtiff 库。

（介绍该图形库的 API 采用怎样一种设计风格，需要经历怎样的流程才能绘制出图形，工作原理是怎样的）

### 数据结构

OpenCV 由一系列 C 函数和少量 C++ 类构成。

#### Scalar

Scalar 是一个从 Vec 类引出的模板类，是一个可存放4个元素的向量，广泛用于传递和读取图像中的像素值。  
四个向量按照BGRA的顺序存储，每个向量为一个 double 类型值。

#### Rect

Rect 由四个数值表示：  
矩形左上角坐标: (x,y)  
矩形的宽和高: width, height

#### Mat
OpenCV 中的图像类。 Mat本质上是由两个数据部分组成的类：
1. 矩阵头(包含信息有通道数、矩阵的大小，用于存储的方法，矩阵存储的地址等)
2. 一个指针，指向包含了像素值的矩阵(可根据选择用于存储的方法采用任何维度存储数据)


### 函数
```c
/*绘制直线函数
img: 要绘制线段的图像。
pt1: 线段的起点。
pt2: 线段的终点。
color: 线段的颜色，通过一个Scalar对象定义。
thickness: 线条的宽度。如果为 -1 则填充形状
lineType: 线段的类型。可以取值8， 4， 和CV_AA， 分别代表8邻接连接线，4邻接连接线和反锯齿连接线。默认值为8邻接。为了获得更好地效果可以选用CV_AA(采用了高斯滤波)。
shift: 坐标点小数点位数
*/
void line(Mat& img, Point pt1, Point pt2, const Scalar& color, int thickness=1, int lineType=8, int shift=0)

/*将两张相同大小，相同类型的图片融合的函数。可以实现 alpha 混合功能
原理为：dst(I)=src1(I)*alpha+src2(I)*beta+gamma
src1: 第一个原数组
alpha: 第一个数组元素权重
src2: 第二个原数组
beta: 第二个数组元素权重
gamma: 图1与图2作和后添加的数值。
dst: 输出图片
*/
void cvAddWeighted( const CvArr* src1, double alpha,const CvArr* src2, double beta,double gamma, CvArr* dst );


```
（列出该库提供的常用函数: 并简单介绍它们的功能）

## 示例

### 绘制线段

```c
void DrawLine(Mat img, Point start, Point end)		//绘制直线
{
  int thickness = 2;
  int lineType = 8;

  line(
    img,	//目标图像
    start,		//直线起点
    end,		//直线终点
    Scalar(0, 0, 0),	//黑色
    thickness,		//线的粗细
    lineType
  );
}
```

### 绘制椭圆

```c
void DrawEllipse(Mat img,double angle)  
{  
    int thickness=2;  
    int lineType=8;  
    ellipse(img,  
        Point(WINDOW_SIZE/2.0,WINDOW_SIZE/2.0),  //椭圆的中心点
        Size(WINDOW_SIZE/4.0,WINDOW_SIZE/16.0),  //椭圆的最小外接矩形
        angle,        //椭圆的旋转角度为angle
        0,            //椭圆拓展的弧度为0到360度
        360,          //椭圆拓展的弧度为0到360度
        Scalar(255,0,255),      //图形颜色
        thickness,              //椭圆的粗度 -1为填充
        lineType);              //线条的类型
}
```

### 透明度混合

```c
//原理为：dst(I)=src1(I)*alpha+src2(I)*beta+gamma
void AlphaBlend()
{  
	cv::Mat img1 = cv::imread("greed.jpg", 1);
	cv::Mat img2 = cv::imread("red.jpg", 1);
	cv::Mat img;
	/* 使用函数进行加权相加 */
	cv::addWeighted(img1, 1.0, img2, 1.0, 0., img, -1);
	/* 或者使用重载运算符进行加权相加 */
	// img = 1.0 * img1 + 1.0 * img2;
	cv::imshow("img", img);
	cv::waitKey(0);
}
```

### 直接操作像素数据

```c
void pixelOperate()
{
  //原始图像初始化
  Mat image(240, 320, CV_8UC3, Scalar(0, 0, 0));
  imshow( "原始图像", image);
  
  double start = static_cast< double>(getTickCount());
  int rowNumber = image.rows; //行数
  int colNumber = image.cols * image.channels(); //每一行元素个数 = 列数 x 通道数
  for ( int i = 0; i < rowNumber; i++) //行循环
  {
    uchar* data = image.ptr<uchar>(i); //获取第i行的首地址
    for ( int j = 0; j < colNumber; j++) //列循环
    {
    //开始处理
    data[j] = 255;
    }
  }
  double end = static_cast< double>(getTickCount());
  double time = (end - start) / getTickFrequency();
  cout << "指针操作运行时间为：" << time << "秒" << endl;
  imshow( "指针操作", image);
}
```
