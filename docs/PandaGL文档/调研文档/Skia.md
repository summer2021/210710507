# Skia

skia是个2D向量图形处理函数库，用于 Chrome 和 Android。它规定2D绘制API，规定图像数据结构，承担编解码调度和软件渲染职责。  
（图形库的简单介绍，包括优缺点、适用场景等）

## 基本概念

Skia 在 Android 中绘制遵循以下绘制流程。  

**Android绘制流程：**  
1. 计算布局，看界面是否需要重新绘制。
2. 计算所有需要更新的 View 的区域，计算其最小外包矩形，即 dirtyRect
3. 锁定一个 Canvas，并获得上一帧非 dirtyRect 的区域。
4. 执行根View的draw方法，递归调用所有子View的onDraw方法。绘制操作最终由skia引擎执行。
5. 完成绘制，解锁 Canvas, 将绘制好的 Buffer 送显。  

**Skia绘制流程：**

1. 指令层  
决定需要哪些绘制操作，绘制操作产生在哪些 layer 上。
2. 解析层  
决定绘制方式，获得需要绘制的对象，进行抗锯齿处理。
3. 渲染层  
产生实际绘制效果，进行透明度混合、采样等操作。

经过源码分析，skia的所有绘制都是通过Path进行绘制的。

### 图像解编码

Skia 使用图形库进行编解码操作。引入的库有: libjpeg, libpng, libgif, webp

### 数据结构

#### 1. SkIRect & SkRect  

skia的矩形数据结构。同时也用来保存椭圆(椭圆的最小外包矩形)。

**数据**  
SKIRect 由四个 int32 型数据表示矩形上下左右四条边的位置，SKRect 由四个 SkScalar(实际为float)表示。  

#### 2. SkColor & SkPMColor

skia的颜色数据。SkPMColor 是为了匹配 kBGRA_8888_SkColorType 而制作的格式。

**数据**  
定义为 uint32_t。
```c
typedef uint32_t SkColor;
```  

**功能**  
1. 获取 alpha 与颜色通道通过位移操作。
```c
#define SkColorGetA(color)      (((color) >> 24) & 0xFF)
#define SkColorGetR(color)      (((color) >> 16) & 0xFF)
#define SkColorGetG(color)      (((color) >>  8) & 0xFF)
#define SkColorGetB(color)      (((color) >>  0) & 0xFF)
```  
2. 设置常用颜色值，如灰色，白色等


#### 3. SkPath

skia 的路径

**数据**  
保存有路径的一系列的端点、控制点，以及路径的样式(如填充和描边、拐角的圆润或尖锐)。

**功能**  
1. 提供常见图形的快捷创建方式，如多边形、圆、椭圆
2. 提供普通的绘制函数，如直线、曲线、矩形等

#### 4. SkPaint

skia 的画笔类

**数据**  
1. **Style:** 填充和描边的样式
2. **Cap:** 路径的首尾样式(圆头方头等)
2. **Join:** 路径拐角处的样式(圆润或者尖锐)

#### 5. SkCanvas

SkCanvas 提供了一个绘图接口，以及如何剪裁和旋转绘图。SkCanvas 和 SkPaint 一起提供了绘制的方式。且SkCanvas中绑定有SkBitmap类型数据。

在创建 SkCanvas 时，首先要先创建raster surface 或者 GPU surface，通过 SkSurface 生成 SkCanvas ，不同的 SkSurface 有不同的绘制接口。   
SkCanvas提供诸如以下的绘制函数
```c
void drawRect(const SkRect& rect, const SkPaint& paint);
```

#### 6. SkBitmap

一个二维的像素数组，同时保存有图片基本信息，以及色彩类型、alpha类型信息。  
每个 SkCanvas 都绑定一个BitMap。

#### 6. SkDevice

一个的虚类，代表设备。内部包含了设备的ImageInfo(宽高色彩范围等)  
实现了将绘制圆角矩形、曲线等图形转换为Path的方法。但绘制Points、Path等方法需要再实现。      
SkCanvas 需要绑定SkDevice类型的输出对象

### 函数
```c
// 将起点移动至指定坐标
SkPath& SkPath::moveTo(SkScalar x, SkScalar y);

// 从当前位置向 (x, y) 画直线
SkPath& SkPath::lineTo(SkScalar x, SkScalar y);

// 使用 paint 中的配置来绘制 path。可以控制填充、描边等
// 在内部实现时，会检测是否为常见图形，并进行绘制加速
SkPath& SkCanvas:drawPath(const SkPath& path, const SkPaint& paint);
```

## 示例

### 绘制线段

```c
void draw(SkCanvas* canvas) {
    SkPaint paint;
    paint.setAntiAlias(true);
    paint.setColor(0xFF9a67be);
    paint.setStrokeWidth(20);
    canvas->skew(1, 0);
    canvas->drawLine(32, 96, 32, 160, paint);
    canvas->skew(-2, 0);
    canvas->drawLine(288, 96, 288, 160, paint);
}

```

### 绘制圆角矩形边框

```c
void draw(SkCanvas* canvas) {
    SkPaint paint;
    paint.setAntiAlias(true);
    paint.setStyle(SkPaint::kStroke_Style);
    paint.setStrokeWidth(8);

    SkPath path;
    double startX = 20, startY = 20; // (startX, startY) 左上角
    double endX = startX + 200, endY = startY + 100; // (endX, endY) 右下角
    double topLeftRadius = 20, topRightRadius = 40;

    // 移动起始点到左上角，并空出左上角圆角位置。 所以x坐标要+topLeftRadius
    path.moveTo(startX + topLeftRadius, startY);

    // 横向画线，同理空出右上角圆角位置
    path.lineTo(endX - topRightRadius, startY);
    // 右上角的圆角， 经过右上角(endX, startY) 向 (endX, startY + topRightRadius) 画曲线
    path.quadTo(endX, startY, endX, startY + topRightRadius);

    // 同理完成右边, 底边，左边的绘制，最后与起始点 (startX + topLeftRadius, startY) 闭合

    // 右边
    path.lineTo(endX, endY - topLeftRadius);
    // 右下圆角
    path.quadTo(endX, endY, endX - topLeftRadius, endY);

    // 底边
    path.lineTo(startX + topRightRadius, endY);
    // 底边圆角
    path.quadTo(startX, endY, startX, endY - topRightRadius);

    // 左边
    path.lineTo(startX, startY + topLeftRadius);
    // 左上圆角
    path.quadTo(startX, startY, startX + topLeftRadius, startY);

    canvas->drawPath(path, paint);
}
```

### 绘制矩形

如果要绘制一个边框与填充颜色不同的矩形，需要进行两边绘制，分别对边框和填充进行绘制。
```javascript
//使用javascript

  // the rectangles
  var rect = SKRect.Create(10, 10, 100, 100);

  // the brush (fill with blue)
  var paint = new SKPaint {
      Style = SKPaintStyle.Fill,
      Color = SKColors.Blue
  };

  // draw fill
  canvas.DrawRect(rect, paint);

  // change the brush (stroke with red)
  paint.Style = SKPaintStyle.Stroke;
  paint.Color = SKColors.Red;

  // draw stroke
  canvas.DrawRect(rect, paint);

```


### 透明度混合

```c
void draw(SkCanvas* canvas) {
  // skia中透明度混合的方式
    SkBlendMode blendModes[] = {
        SkBlendMode::kDst,
        SkBlendMode::kSrc,
        SkBlendMode::kSrcOver,
        SkBlendMode::kDstOver,
        SkBlendMode::kSrcIn,
        SkBlendMode::kDstIn,
        SkBlendMode::kSrcOut,
        SkBlendMode::kDstOut,
        SkBlendMode::kSrcATop,
        SkBlendMode::kDstATop,
        SkBlendMode::kXor,
        SkBlendMode::kPlus,
        SkBlendMode::kModulate,
        SkBlendMode::kScreen,
        SkBlendMode::kOverlay,
        SkBlendMode::kDarken,
        SkBlendMode::kLighten,
    };

    SkPaint labelPaint;
    labelPaint.setAntiAlias(true);
    SkFont font(nullptr, 12);

    for (auto mode : blendModes) {
        SkPaint layerPaint;
        layerPaint.setBlendMode(mode);

        canvas->save();
        canvas->clipRect(SkRect::MakeWH(256, 256));

        drawBG(canvas);

        canvas->saveLayer(nullptr, &layerPaint);
        const SkScalar r = 80;
        SkPaint discP;
        discP.setAntiAlias(true);
        //设置混色模式
        discP.setBlendMode(SkBlendMode::kPlus);

        //绘制底图
        discP.setColor(SK_ColorGREEN); canvas->drawCircle(128, r, r, discP);
        discP.setColor(SK_ColorRED);   canvas->drawCircle(r, 256 - r, r, discP);
        discP.setColor(SK_ColorBLUE);  canvas->drawCircle(256 - r, 256 - r, r, discP);
        canvas->restore();

        //绘制顶部图案
        canvas->drawSimpleText(SkBlendMode_Name(mode), strlen(SkBlendMode_Name(mode)),
                               SkTextEncoding::kUTF8, 10, 10, font, labelPaint);
        canvas->restore();
        canvas->translate(0, 256);
    }
}
```

### 直接操作像素数据

```c
void draw(SkCanvas* canvas) {
    constexpr int width = 4;
    constexpr int height = 4;
    static const uint32_t pixels[height * width] = {
        0xFF000000, 0xFF005500, 0xFF00AA00, 0xFF00FF00,
        0xFF000055, 0xFF005555, 0xFF00AA55, 0xFF00FF55,
        0xFF0000AA, 0xFF0055AA, 0xFF00AAAA, 0xFF00FFAA,
        0xFF0000FF, 0xFF0055FF, 0xFF00AAFF, 0xFF00FFFF,
    };
    SkPixmap pixmap(SkImageInfo::Make(width, height, kN32_SkColorType, kPremul_SkAlphaType),
                    pixels, sizeof(uint32_t) * width);
    sk_sp<SkImage> img = SkImage::MakeRasterCopy(pixmap);

    canvas->scale(16, 16);
    canvas->drawImage(img, 6, 6);
}
```
