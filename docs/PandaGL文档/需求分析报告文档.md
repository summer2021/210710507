## LCUI图形部分需求分为几部分：
1.	绘制直线功能
2.	绘制矩形功能
3.	绘制圆角矩形
4.	读取与绘制图片
5.	盒阴影功能
6.	基础图形功能，为上述功能提供支持
## LCUI现有图形功能

### 1
**功能函数:** draw/line.c/Graph_DrawHorizLine & Graph_DrawVertiLine  
**输入数据:** LCUI_Graph图形对象 颜色 宽度 起始点 长度  
**说明:** 画水平或竖直线段

### 2
**功能函数:** draw/background.c/Background_Paint  
**输入数据:** LCUI_Background LCUI_Rect LCUI_PaintContext  
**说明:** 绘制背景图片

### 3
**功能函数:** Draw/border.c/Border_Paint & DrawBorder__ & CropContentTopRight  
**输入数据:** LCUI_Border LCUI_Rect LCUI_PaintContext  
**说明:** 绘制圆角边框。分为四个角绘制

### 4
**功能函数:** Draw/boxshadow.c/BoxShadow_Paint  
**输入数据:** LCUI_BoxShadow LCUI_Rect 尺寸 LCUI_PaintContext  
**说明:** 矩形框阴影绘制

### 5
**功能函数:** Graph.c/Graph___Flip  
**输入数据:** LCUI_Graph  
**说明:** 水平、竖直方向反转

### 5
**功能函数:** Graph.c/ Graph_Zoom  
**输入数据:** 图形对象 缓冲 是否保持长宽比 尺寸  
**说明:** 变形函数

### 6
**功能:** 多渲染后端  
**说明:** 使得同样一段代码能够在不同平台运行的能力。默认使用 CPU 作为渲染后端, 其它后端的添加方法可在文档中讲述

### 7
**功能:** 局部绘制  
**说明:** 能够局部绘制图形。比如绘制半个矩形、四分之一个圆等。

### 8
**功能:** 硬件加速支持  
**说明:** 不需要开发硬件加速相关功能，但需要了解大致的实现方法和注意事项，避免设计出与硬件加速有冲突的代码。

### 其他函数
**说明:** 如填充、渐变、获取与设置基本信息等其他功能函数
