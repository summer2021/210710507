#ifndef PANDAGL_BASE_DEVICE_H
#define PANDAGL_BASE_DEVICE_H

#include "PandaGL/graph/Rect.h"
#include "PandaGL/graph/Line.h"
#include "PandaGL/draw/Paint.h"
#include "PandaGL/draw/Bitmap.h"

namespace pandagl
{
	class BaseDevice
	{
	public:
		virtual void drawLine(int x0, int y0, LineType line_type, int length, const Paint& paint) = 0;
		virtual void drawRect(int32_t x0, int32_t y0, int32_t x1, int32_t y1, const Paint& paint) = 0;

		Bitmap* bitmap;
	};
}

#endif // !PANDAGL_BASE_DEVICE_H