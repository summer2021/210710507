#ifndef PANDAGL_DEFAULT_DEVICE_H
#define PANDAGL_DEFAULT_DEVICE_H

#include "BaseDevice.h"

namespace pandagl
{
	class DefaultDevice :public BaseDevice
	{
	public:
		void drawLine(int32_t x0, int32_t y0, LineType line_type, int32_t length, const Paint& paint);
		void drawRect(int32_t x0, int32_t y0, int32_t width, int32_t height, const Paint& paint);
	};
}

#endif // !PANDAGL_BASE_DEVICE_H