#ifndef PANDAGL_BITMAP_H
#define PANDAGL_BITMAP_H

#include "Color_32.h"

namespace pandagl
{
	class Bitmap
	{
	public:
		Bitmap() { width = 0; height = 0; data = NULL; };

		void Config(uint32_t width, uint32_t height);
		void AllocPixels();

		uint32_t width;
		uint32_t height;
		Color_32* data;
	};
}

#endif // !PANDAGL_BITMAP_H