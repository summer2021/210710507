#ifndef PANDAGL_CANVAS_H
#define PANDAGL_CANVAS_H

#include "Bitmap.h"
#include "PandaGL/device/DefaultDevice.h"
#include "PandaGL/draw/Paint.h"

namespace pandagl
{
	/// @brief Container to place graphs, a special type of Graph
	class Canvas 
	{
	public:
		/// @brief Constructor
		Canvas() {
			device = new DefaultDevice();
		};

		/// @brief Constructor
		Canvas(BaseDevice* device) {
			this->device = device;
		};

		/// @brief Draw line segment from (x0, y0) to (x1, y1)
		/// @param x0		start of line segment on x-axis
		/// @param y0		start of line segment on y-axis
		/// @param x1		end of line segment on x-axis
		/// @param y1		end of line segment on y-axis
		/// @param paint	color, draw style and so on, used to draw
		void DrawLine(int32_t x0, int32_t y0, LineType line_type, int32_t length, const Paint& paint);

		void DrawLine(const Line& line, const Paint& paint)
		{
			this->DrawLine(line.pos.x, line.pos.y, line.line_type, line.length, paint);
		}

		/// @brief Draw rectangle
		/// @param x0		left-top point of rect on the x-axis
		/// @param y0		left-top point of rect on the y-axis
		/// @param width	pixel column count; zero or greater
		/// @param height	pixel row count; zero or greater
		/// @param paint	color, draw style and so on, used to draw
		void DrawRect(int32_t x0, int32_t y0, int32_t width, int32_t height, const Paint& paint);

		void DrawRect(const Rect& rect, const Paint& paint)
		{
			this->DrawRect(rect.pos.x, rect.pos.y, rect.width, rect.height, paint);
		}






		BaseDevice* device;

	};
}

#endif // !PANDAGL_CANVAS_H