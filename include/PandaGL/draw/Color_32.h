#ifndef PANDAGL_COLOR_32_H
#define PANDAGL_COLOR_32_H

#include "../types.h"

namespace pandagl
{
	union Color_32 {
		uint32_t value;
		struct {
			unsigned char a;
			unsigned char r;
			unsigned char g;
			unsigned char b;
		};

		/// @brief Constructor
		Color_32() { a = r = g = b = 0; }

		/// @brief Constructor
		static Color_32 FromARGB(u_char a, u_char r, u_char g, u_char b)
		{
			Color_32 c;
			c.a = a;
			c.r = r;
			c.g = g;
			c.b = b;
			return c;
		}

		/// @brief Blend two color with alpha value
		/// @param foreground Foreground color
		/// @param background Background color
		/// @return blended value
		static Color_32 AlphaBlend(Color_32 foreground, Color_32 background)
		{
			u_char alpha = foreground.a;
			if (alpha == 0x00) { // Foreground completely transparent.
				return background;
			}
			u_char invAlpha = 0xff - alpha;
			u_char backAlpha = background.a;
			if (backAlpha == 0xff) { // Opaque background case
				Color_32 color;
				color.a = 0xff;
				color.r = ((u_char)(alpha * foreground.r + invAlpha * background.r)) >> 8;
				color.g = ((u_char)(alpha * foreground.g + invAlpha * background.g)) >> 8;
				color.b = ((u_char)(alpha * foreground.b + invAlpha * background.b)) >> 8;
				return color;
			}
			else { // General case
				backAlpha = (backAlpha * invAlpha) >> 8;
				u_char outAlpha = alpha + backAlpha;
				Color_32 color;
				color.a = outAlpha;
				color.r = (foreground.r * alpha + background.r * backAlpha) / outAlpha;
				color.g = (foreground.g * alpha + background.g * backAlpha) / outAlpha;
				color.b = (foreground.b * alpha + background.b * backAlpha) / outAlpha;
				return color;
			}
		}

		/// @brief Blend two color and Assign value to background. much quicker than alphaBlend
		/// @param foreground Foreground color
		/// @param background Background color
		static void AlphaBlendToBack(const Color_32& foreground, Color_32 & background)
		{
			u_char alpha = foreground.a;
			if (alpha == 0x00) { // Foreground completely transparent.
				return;
			}
			u_char invAlpha = 0xff - alpha;
			u_char backAlpha = background.a;
			if (backAlpha == 0xff) { // Opaque background case
				background.a = 0xff;
				background.r = ((alpha * foreground.r + invAlpha * background.r)) >> 8;
				background.g = ((alpha * foreground.g + invAlpha * background.g)) >> 8;
				background.b = ((alpha * foreground.b + invAlpha * background.b)) >> 8;
				return;
			}
			if (backAlpha == 0x00)
			{
				return;
			}
			else { // General case
				backAlpha = (backAlpha * invAlpha) >> 8;
				u_char outAlpha = alpha + backAlpha;
				background.a = outAlpha;
				background.r = (foreground.r * alpha + background.r * backAlpha) / outAlpha;
				background.g = (foreground.g * alpha + background.g * backAlpha) / outAlpha;
				background.b = (foreground.b * alpha + background.b * backAlpha) / outAlpha;
				return;
			}
		}
	};
}

#endif // !PANDAGL_COLOR_32_H