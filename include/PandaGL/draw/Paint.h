#ifndef PANDAGL_PAINT_H
#define PANDAGL_PAINT_H

#include "Color_32.h"

namespace pandagl
{
	enum PaintStyle {
		FILL,          //!< set to fill geometry
		STROKE,        //!< set to stroke geometry
		FILL_AND_STROKE, //!< sets to fill and stroke geometry
	};

	class Paint
	{
	public:

		bool LineDrawable() const
		{
			if (line_width > 0)
				return true;
			return false;
		}

		void setARGB(u_char a, u_char r, u_char g, u_char b)
		{
			color.a = a;
			color.r = r;
			color.g = g;
			color.b = b;
		}

		void setColor(Color_32 paint_color)
		{
			color = paint_color;
		}

		Color_32 color;

		int line_width;

		PaintStyle style;

	};
}




#endif // !PANDAGL_PAINT_H