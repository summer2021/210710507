#ifndef PANDAGL_CIRCLE_H
#define PANDAGL_CIRCLE_H

#include "Graph.h"

namespace pandagl
{
	class Circle :public Graph
	{
	public:
		/// @brief Constructor
		Circle() { this->radius = 0; type = ClassType::CIRCLE; };
		Circle(int radius)
			 {
			this->radius = radius;
			type = ClassType::CIRCLE;
		};

		int radius;
	};
}

#endif // !PANDAGL_CIRCLE_H