#ifndef PANDAGL_ELLIPSE_H
#define PANDAGL_ELLIPSE_H

#include "Graph.h"

namespace pandagl
{
	class Ellipse :public Graph
	{
	public:
		/// @brief Constructor
		Ellipse() {
			width = 0;
			height = 0;
			type = ClassType::ELLIPSE;
		};
		Ellipse(int width, int height)
		{
			this->width = width;
			this->height = height;
			type = ClassType::ELLIPSE;
		};

		/// @brief Length of the semi-axis in the horizontal direction
		int width;
		/// @brief Length of the semi-axis in the vertical direction
		int height;
	};
}
#endif // !PANDAGL_ELLIPSE_H