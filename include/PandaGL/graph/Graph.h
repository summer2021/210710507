#ifndef PANDAGL_GRAPH_H
#define PANDAGL_GRAPH_H

#include "Point.h"
#include "../types.h"

namespace pandagl
{
	/// @brief Base class of all the graphics classes
	class Graph
	{
	public:
		/// @brief Constructor
		Graph() { pos = Point(); type = ClassType::GRAPH; };
		Graph(Point p) { pos = p; type = ClassType::GRAPH; };

		/// @brief The pixel position of the anchor point.
		/// (For straight lines and rectangles, it is the upper left corner of the image;
		///  for ellipses, it is the center of the circle)
		Point pos;
		ClassType type;
	};
}

#endif // !PANDAGL_GRAPH_H