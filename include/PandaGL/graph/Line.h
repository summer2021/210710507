#ifndef PANDAGL_LINE_H
#define PANDAGL_LINE_H

#include "Graph.h"

namespace pandagl
{
	enum LineType
	{
		HORIZONTAL,
		VERTICAL
	};

	/// @brief Simple segment
	class Line :public Graph
	{
	public:
		/// @brief Constructor
		Line() {
			line_type = LineType::HORIZONTAL;
			length = 0;
			type = ClassType::LINE;
		};
		Line(LineType line_type, int32_t length) {
			this->line_type = line_type;
			this->length = length;
			type = ClassType::LINE;
		};


		LineType line_type;			//LineType
		int32_t length;
	};
}

#endif // !PANDAGL_LINE_H