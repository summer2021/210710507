#ifndef PANDAGL_POINT_H
#define PANDAGL_POINT_H

namespace pandagl
{
	/// @brief Record point posit
	class Point
	{
	public:
		/// @brief Constructor
		Point() { x = 0; y = 0; };
		Point(int x, int y) { this->x = x; this->y = y; };

		int x;
		int y;
	};
}

#endif // !PANDAGL_POINT_H
