#ifndef PANDAGL_RECT_H
#define PANDAGL_RECT_H

#include "Graph.h"

namespace pandagl
{
	class Rect :public Graph
	{
	public:
		/// @brief Constructor
		Rect() {
			width = 0;
			height = 0;
			type = ClassType::RECT;
		};
		Rect(int width, int height) {
			this->width = width;
			this->height = height;
			type = ClassType::RECT;
		};

		int width;
		int height;
	};
}

#endif // !PANDAGL_RECT_H