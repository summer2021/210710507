#ifndef PANDAGL_ROUND_RECT_H
#define PANDAGL_ROUND_RECT_H

#include "Graph.h"

namespace pandagl
{
	class RoundRect :public Graph
	{
	public:
		/// @brief Constructor
		RoundRect() {
			width = 0;
			height = 0;
			type = ClassType::ROUNDED_RECT;
		};
		RoundRect(float width, float height)
			{
			this->width = width;
			this->height = width;
			type = ClassType::ROUNDED_RECT;
		};


		float width;
		float height;

	};
}

#endif // !PANDAGL_ROUND_RECT_H