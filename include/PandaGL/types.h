#ifndef PANDAGL_TYPES_H
#define PANDAGL_TYPES_H

#include <vector>
#include <list>
#include <iostream>
using namespace std;

typedef unsigned char u_char;

namespace pandagl
{
	enum ClassType
	{
		GRAPH,
		CANVAS,
		LINE,
		ELLIPSE,
		CIRCLE,
		RECT,
		ROUND_RECT,
	};
}


#endif // !TYPES_H