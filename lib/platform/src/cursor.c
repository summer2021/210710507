﻿/* cursor.c -- Mouse cursor operation set.
 *
 * Copyright (c) 2018, Liu chao <lc-soft@live.cn> All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *   * Neither the name of LCUI nor the names of its contributors may be used
 *     to endorse or promote products derived from this software without
 *     specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <LCUI_Build.h>
#include <LCUI/LCUI.h>
#include <LCUI/cursor.h>
#include <LCUI/display.h>

static struct LCUICursorModule {
	pd_pos_t pos;      /* 当前帧的坐标 */
	pd_pos_t new_pos;  /* 下一帧将要更新的坐标 */
	LCUI_BOOL visible; /* 是否可见 */
	pd_canvas_t graph;  /* 游标的图形 */
} cursor;

static uchar_t cursor_img_rgba[4][12 * 19] = {
	{ 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3,
	3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 229, 3, 3,
	0, 0, 0, 0, 0, 0, 0, 0, 3, 255, 229, 3, 3, 0,
	0, 0, 0, 0, 0, 0, 3, 255, 255, 229, 3, 3, 0, 0,
	0, 0, 0, 0, 3, 255, 255, 255, 229, 3, 3, 0, 0, 0,
	0, 0, 3, 255, 255, 255, 255, 229, 3, 3, 0, 0, 0, 0,
	3, 255, 255, 255, 255, 253, 226, 3, 3, 0, 0, 0, 3, 255,
	255, 255, 253, 251, 248, 220, 3, 3, 0, 0, 3, 255, 255, 253,
	251, 248, 245, 241, 214, 3, 3, 0, 3, 255, 253, 251, 248, 245,
	241, 238, 234, 207, 3, 3, 3, 253, 251, 248, 245, 241, 238, 234,
	230, 226, 201, 3, 3, 251, 248, 245, 217, 238, 234, 3, 3, 3,
	3, 3, 3, 248, 245, 217, 3, 164, 230, 3, 3, 0, 0, 0,
	3, 245, 217, 3, 3, 3, 226, 201, 3, 0, 0, 0, 3, 217,
	3, 3, 0, 3, 176, 219, 3, 3, 0, 0, 3, 3, 3, 0,
	0, 3, 3, 216, 192, 3, 0, 0, 0, 0, 0, 0, 0, 0,
	3, 192, 211, 3, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3,
	3, 3, 0, 0 },
	{ 6, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6,
	6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 230, 6, 6,
	0, 0, 0, 0, 0, 0, 0, 0, 6, 255, 230, 6, 6, 0,
	0, 0, 0, 0, 0, 0, 6, 255, 255, 230, 6, 6, 0, 0,
	0, 0, 0, 0, 6, 255, 255, 255, 230, 6, 6, 0, 0, 0,
	0, 0, 6, 255, 255, 255, 255, 230, 6, 6, 0, 0, 0, 0,
	6, 255, 255, 255, 255, 253, 226, 6, 6, 0, 0, 0, 6, 255,
	255, 255, 253, 251, 248, 221, 6, 6, 0, 0, 6, 255, 255, 253,
	251, 248, 245, 241, 214, 6, 6, 0, 6, 255, 253, 251, 248, 245,
	241, 238, 234, 207, 6, 6, 6, 253, 251, 248, 245, 241, 238, 234,
	230, 226, 201, 6, 6, 251, 248, 245, 217, 238, 234, 6, 6, 6,
	6, 6, 6, 248, 245, 217, 6, 165, 230, 6, 6, 0, 0, 0,
	6, 245, 217, 6, 6, 6, 226, 201, 6, 0, 0, 0, 6, 217,
	6, 6, 0, 6, 176, 219, 6, 6, 0, 0, 6, 6, 6, 0,
	0, 6, 6, 216, 192, 6, 0, 0, 0, 0, 0, 0, 0, 0,
	6, 192, 211, 6, 0, 0, 0, 0, 0, 0, 0, 0, 6, 6,
	6, 6, 0, 0 },
	{ 26, 26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 26,
	26, 0, 0, 0, 0, 0, 0, 0, 0, 0, 26, 232, 26, 26,
	0, 0, 0, 0, 0, 0, 0, 0, 26, 255, 232, 26, 26, 0,
	0, 0, 0, 0, 0, 0, 26, 255, 255, 232, 26, 26, 0, 0,
	0, 0, 0, 0, 26, 255, 255, 255, 232, 26, 26, 0, 0, 0,
	0, 0, 26, 255, 255, 255, 255, 232, 26, 26, 0, 0, 0, 0,
	26, 255, 255, 255, 255, 253, 228, 26, 26, 0, 0, 0, 26, 255,
	255, 255, 253, 251, 248, 223, 26, 26, 0, 0, 26, 255, 255, 253,
	251, 248, 245, 241, 216, 26, 26, 0, 26, 255, 253, 251, 248, 245,
	241, 238, 234, 209, 26, 26, 26, 253, 251, 248, 245, 241, 238, 234,
	230, 226, 203, 26, 26, 251, 248, 245, 219, 238, 234, 26, 26, 26,
	26, 26, 26, 248, 245, 219, 26, 171, 230, 26, 26, 0, 0, 0,
	26, 245, 219, 26, 26, 26, 226, 203, 26, 0, 0, 0, 26, 219,
	26, 26, 0, 26, 181, 219, 26, 26, 0, 0, 26, 26, 26, 0,
	0, 26, 26, 216, 194, 26, 0, 0, 0, 0, 0, 0, 0, 0,
	26, 194, 211, 26, 0, 0, 0, 0, 0, 0, 0, 0, 26, 26,
	26, 26, 0, 0 },
	{ 231, 55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 231, 189,
	55, 0, 0, 0, 0, 0, 0, 0, 0, 0, 231, 255, 189, 55,
	0, 0, 0, 0, 0, 0, 0, 0, 231, 255, 255, 189, 55, 0,
	0, 0, 0, 0, 0, 0, 231, 255, 255, 255, 189, 55, 0, 0,
	0, 0, 0, 0, 231, 255, 255, 255, 255, 189, 55, 0, 0, 0,
	0, 0, 231, 255, 255, 255, 255, 255, 189, 55, 0, 0, 0, 0,
	231, 255, 255, 255, 255, 255, 255, 189, 55, 0, 0, 0, 231, 255,
	255, 255, 255, 255, 255, 255, 189, 55, 0, 0, 231, 255, 255, 255,
	255, 255, 255, 255, 255, 189, 55, 0, 231, 255, 255, 255, 255, 255,
	255, 255, 255, 255, 189, 55, 231, 255, 255, 255, 255, 255, 255, 255,
	255, 255, 255, 189, 231, 255, 255, 255, 255, 255, 255, 189, 189, 189,
	189, 189, 231, 255, 255, 255, 244, 255, 255, 188, 77, 0, 0, 0,
	231, 255, 255, 244, 55, 211, 255, 255, 211, 0, 0, 0, 231, 255,
	244, 55, 0, 180, 255, 255, 180, 77, 0, 0, 189, 244, 55, 0,
	0, 55, 215, 255, 255, 209, 0, 0, 0, 0, 0, 0, 0, 0,
	180, 255, 255, 204, 0, 0, 0, 0, 0, 0, 0, 0, 26, 215,
	158, 49, 0, 0 }
};

static int LCUICursor_LoadDefualtGraph(pd_canvas_t *buff)
{
	if (pd_canvas_is_valid(buff)) {
		pd_canvas_free(buff);
	}
	pd_canvas_init(buff);
	buff->color_type = PD_COLOR_TYPE_ARGB;
	if (pd_canvas_create(buff, 12, 19) != 0) {
		return -1;
	}
	pd_canvas_set_red_bits(buff, cursor_img_rgba[0], 12 * 19);
	pd_canvas_set_green_bits(buff, cursor_img_rgba[1], 12 * 19);
	pd_canvas_set_blue_bits(buff, cursor_img_rgba[2], 12 * 19);
	pd_canvas_set_alpha_bits(buff, cursor_img_rgba[3], 12 * 19);
	return 0;
}

static void OnMouseMoveEvent(LCUI_SysEvent e, void *arg)
{
	cursor.new_pos.x = e->motion.x;
	cursor.new_pos.y = e->motion.y;
}

void LCUI_InitCursor(void)
{
	pd_canvas_t pic;
	pd_canvas_init(&pic);
	pd_canvas_init(&cursor.graph);
	/* 载入自带的游标的图形数据 */
	LCUICursor_LoadDefualtGraph(&pic);
	cursor.new_pos.x = LCUIDisplay_GetWidth() / 2;
	cursor.new_pos.y = LCUIDisplay_GetHeight() / 2;
	LCUI_BindEvent(LCUI_MOUSEMOVE, OnMouseMoveEvent, NULL, NULL);
	LCUICursor_SetGraph(&pic);
	LCUICursor_Show();
	pd_canvas_free(&pic);
}

void LCUI_FreeCursor(void)
{
	pd_canvas_free(&cursor.graph);
}

void LCUICursor_GetRect(pd_rect_t *rect)
{
	float scale;
	scale = LCUIMetrics_GetScale();
	rect->x = y_iround(cursor.pos.x / scale);
	rect->y = y_iround(cursor.pos.y / scale);
	rect->width = y_iround(cursor.graph.width / scale);
	rect->height = y_iround(cursor.graph.height / scale);
}

void LCUICursor_Refresh(void)
{
	pd_rect_t rect;
	if (!cursor.visible) {
		return;
	}
	LCUICursor_GetRect(&rect);
	LCUIDisplay_InvalidateArea(&rect);
}

LCUI_BOOL LCUICursor_IsVisible(void)
{
	return cursor.visible;
}

void LCUICursor_Show(void)
{
	cursor.visible = TRUE;
	LCUICursor_Refresh();
}

void LCUICursor_Hide(void)
{
	LCUICursor_Refresh();
	cursor.visible = FALSE;
}

void LCUICursor_Update(void)
{
	if (cursor.pos.x == cursor.new_pos.x &&
	    cursor.pos.y == cursor.new_pos.y) {
		return;
	}
	LCUICursor_Refresh();
	cursor.pos = cursor.new_pos;
	LCUICursor_Refresh();
}

/* 设定游标的位置 */
void LCUICursor_SetPos(pd_pos_t pos)
{
	cursor.new_pos = pos;
}

/** 设置游标的图形 */
int LCUICursor_SetGraph(pd_canvas_t *graph)
{
	if (pd_canvas_is_valid(graph)) {
		LCUICursor_Refresh();
		if (pd_canvas_is_valid(&cursor.graph)) {
			pd_canvas_free(&cursor.graph);
		}
		pd_canvas_copy(&cursor.graph, graph);
		LCUICursor_Refresh();
		return 0;
	}
	return -1;
}

/* 获取鼠标指针当前的坐标 */
void LCUICursor_GetPos(pd_pos_t *pos)
{
	*pos = cursor.new_pos;
}

int LCUICursor_Paint(pd_paint_context_t* paint)
{
	int x, y;
	if (!cursor.visible) {
		return 0;
	}
	x = cursor.pos.x - paint->rect.x;
	y = cursor.pos.y - paint->rect.y;
	return pd_canvas_mix(&paint->canvas, &cursor.graph, x, y, FALSE);
}
