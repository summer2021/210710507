set -e
PACKAGE_NAME=lcui
PACKAGE_VERSION=2.2.0
PACKAGE_NAME_WITH_VERSION=$PACKAGE_NAME-$PACKAGE_VERSION
PACKAGE_TAR=$PACKAGE_NAME_WITH_VERSION.tar
PACKAGE_FILE=$PACKAGE_TAR.gz
PACKAGE_ORIG_FILE=${PACKAGE_NAME}_${PACKAGE_VERSION}.orig.tar.gz
DEBEMAIL="lc-soft@live.cn"
DEBFULLNAME="Liu Chao"
git archive --prefix $PACKAGE_NAME_WITH_VERSION/ -o $PACKAGE_TAR HEAD
gzip $PACKAGE_TAR $PACKAGE_FILE
[ -d build/debian ] || mkdir build/debian
cd build/debian
[ -f $PACKAGE_ORIG_FILE ] && rm $PACKAGE_ORIG_FILE
[ -d $PACKAGE_NAME_WITH_VERSION ] && rm -rf $PACKAGE_NAME_WITH_VERSION
mv ../../$PACKAGE_FILE ./$PACKAGE_ORIG_FILE
tar xf $PACKAGE_ORIG_FILE
cd $PACKAGE_NAME_WITH_VERSION
dpkg-buildpackage --unsigned-changes --unsigned-source
