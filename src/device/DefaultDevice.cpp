#include "..\..\include\PandaGL\device\DefaultDevice.h"
#include <iostream>

namespace pandagl
{
	void DefaultDevice::drawLine(int32_t x0, int32_t y0, LineType line_type, int32_t length, const Paint& paint)
	{
		int32_t width = this->bitmap->width;
		int32_t height = this->bitmap->height;
		int32_t begin_pos = y0 * width + x0;

		int32_t offset = paint.frame_width / -2;
		// draw horizontal line
		if (line_type == LineType::HORIZONTAL)
		{
			for (int32_t i = offset; i < offset + paint.frame_width; i++)
			{
				// for each line
				for (int32_t j = 0; j < length; j++)
				{
					// for each row
					if (x0 + j < width && x0 + j > 0 && y0 + i < height && y0 + i >0)
					{
						if (this->bitmap->data[begin_pos + i * width + j].a == 0x00)
							this->bitmap->data[begin_pos + i * width + j] = paint.color;
						else
							Color_32::AlphaBlendToBack(paint.color, this->bitmap->data[begin_pos + i * width + j]);
					}
						
				}
			}
		}
		// draw vertical line
		else
		{
			for (int32_t i = offset; i < offset + paint.frame_width; i++)
			{
				// for each row
				for (int32_t j = 0; j < length; j++)
				{
					//for each line
					if (x0 + i < width && x0 + i > 0 && y0 + j < height && y0 + j > 0)
					{
						if (this->bitmap->data[begin_pos + j * width + i].a == 0x00)
							this->bitmap->data[begin_pos + j * width + i] = paint.color;
						else
						{
							Color_32::AlphaBlendToBack(paint.color, this->bitmap->data[begin_pos + j * width + i]);
						}
					}
				}
			}
		}
	}

	void DefaultDevice::drawRect(int32_t x0, int32_t y0, int32_t rect_width, int32_t rect_height, const Paint& paint)
	{
		if (paint.style == PaintStyle::STROKE)
		{
			this->drawLine(x0 - paint.frame_width / 2, y0, LineType::HORIZONTAL, rect_width + paint.frame_width, paint);
			this->drawLine(x0 - paint.frame_width / 2, y0 + rect_height, LineType::HORIZONTAL, rect_width + paint.frame_width, paint);
			this->drawLine(x0 , y0 + paint.frame_width / 2, LineType::VERTICAL, rect_height - paint.frame_width, paint);
			this->drawLine(x0 + rect_width, y0 + paint.frame_width / 2, LineType::VERTICAL, rect_height - paint.frame_width, paint);
			return;
		}//end style==STROKE
		if (paint.style == PaintStyle::FILL)
		{
			int32_t bitmap_width = this->bitmap->width;
			int32_t bitmap_height = this->bitmap->height;
			int32_t begin_pos = y0 * this->bitmap->width + x0;

			int32_t offset = paint.frame_width / 2;

			for (int i = x0 + offset; i < x0 + rect_width - offset; i++)
			{
				for (int j = y0 + offset; j < y0 + rect_height - offset; j++)
				{
					if (this->bitmap->data[i + j * bitmap_width].a == 0x00)
						this->bitmap->data[i + j * bitmap_width] = paint.color;
					else
					{
						Color_32::AlphaBlendToBack(paint.color, this->bitmap->data[i + j * bitmap_width]);
					}
				}
			}
		}//end style==FILL
		if (paint.style == PaintStyle::FILL_AND_STROKE)
		{
			int32_t bitmap_width = this->bitmap->width;
			int32_t bitmap_height = this->bitmap->height;
			int32_t begin_pos = y0 * this->bitmap->width + x0;

			int32_t offset = paint.frame_width / -2;

			for (int i = x0 + offset; i < x0 + rect_width - offset; i++)
			{
				for (int j = y0 + offset; j < y0 + rect_height - offset; j++)
				{
					if (this->bitmap->data[i + j * bitmap_width].a == 0x00)
						this->bitmap->data[i + j * bitmap_width] = paint.color;
					else
					{
						Color_32::AlphaBlendToBack(paint.color, this->bitmap->data[i + j * bitmap_width]);
					}
				}
			}
		}//end style==FILL


	}

}



