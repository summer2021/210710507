#include "..\..\include\PandaGL\draw\Canvas.h"

namespace pandagl
{
	void Canvas::DrawLine(int32_t x0, int32_t y0, LineType line_type, int32_t length, const Paint& paint)
	{
		if (paint.LineDrawable() == false)
			return;
		this->device->drawLine(x0, y0, line_type, length, paint);
	}

	void Canvas::DrawRect(int32_t x0, int32_t y0, int32_t x1, int32_t y1, const Paint& paint)
	{
		this->device->drawRect(x0, y0, x1, y1, paint);
	}

}