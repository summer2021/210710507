#include "include.h"
#include <time.h>

using namespace pandagl;

void encodePng(const char* filename, const Bitmap& bitmap)
{
	vector<unsigned char> imageData;
	for (int i = 0; i < bitmap.height; i++)
	{
		for (int j = 0; j < bitmap.width; j++)
		{
			imageData.push_back((u_char)bitmap.data[bitmap.width * i + j].r);
			imageData.push_back((u_char)bitmap.data[bitmap.width * i + j].g);
			imageData.push_back((u_char)bitmap.data[bitmap.width * i + j].b);
			imageData.push_back((u_char)bitmap.data[bitmap.width * i + j].a);
		}
	}


	//Encode the image
	unsigned error = lodepng::encode(filename, imageData, bitmap.width, bitmap.height);

	//if there's an error, display it
	if (error) std::cout << "encoder error " << error << ": " << lodepng_error_text(error) << std::endl;
}

//void testAlphaBlendTime()
//{
//	int width = 4000;
//	clock_t start;
//	clock_t end;
//	Image image1,image2;
//	image1.width = image1.height = width;
//	image1.data = new Color_32[width * width];
//	image2.width = image2.height = width;
//	image2.data = new Color_32[width * width];
//
//	for (int i = 0; i < width; i++)
//	{
//		for (int j = 0; j < width; j++)
//		{
//			image2.data[i * image2.width + j].r = i % 0xff;
//			image2.data[i * image2.width + j].g = j % 0xff;
//			image2.data[i * image2.width + j].b = i % 0xff;
//		}
//	}
//
//	u_char alpha = 0x22;
//	start = clock();
//	for (int m = 0; m < 10; m++)
//	{
//		if (++alpha > 0xff)
//			alpha = 0;
//		for (int i = 0; i < width; i++)
//		{
//			for (int j = 0; j < width; j++)
//			{
//				int pos = i * image2.width + j;
//				image2.data[pos].a = alpha;
//
//
//				//image1.data[pos] = Color_32::alphaBlend(image2.data[pos], image1.data[pos]);
//				//alphaBlend2(&image2.data[pos], &image1.data[pos]);
//				Color_32::alphaBlendToBack(&image2.data[pos], &image1.data[pos]);
//				//
//			}
//		}
//	}
//	end = clock();
//	double endtime = (double)(end - start) / CLOCKS_PER_SEC;
//	cout << "Total time:" << endtime  << "s" << endl;
//
//	vector<unsigned char> imageData;
//	for (int i = 0; i < width; i++)
//	{
//		for (int j = 0; j < width; j++)
//		{
//			int pos = i * image2.width + j;
//			imageData.push_back(image2.data[pos].r);
//			imageData.push_back(image2.data[pos].g);
//			imageData.push_back(image2.data[pos].b);
//			imageData.push_back(image2.data[pos].a);
//		}
//	}
//	encodePng2("testout.png", imageData, width, width);
//}

void drawLine()
{
	//Create and config bitmap
	Bitmap bitmap;
	bitmap.Config(1920, 1080);
	bitmap.AllocPixels();

	//Create device and link with bitmap
	DefaultDevice device;
	device.bitmap = &bitmap;

	//Create canvas and link with device
	Canvas canvas(&device);

	//Set Paint style
	Paint paint;
	paint.setARGB(255, 255, 0, 0);		//color
	paint.line_width = 4;

	//Draw canvas with paint. Then data in bitmap is drawed.
	canvas.DrawLine(40, 80, LineType::HORIZONTAL, 400, paint);

	//Change color of paint
	paint.setARGB(60, 0, 255, 0);

	//Draw another line
	canvas.DrawLine(80, 40, LineType::VERTICAL, 400, paint);



	//for (int i = 0; i < canvas.device->bitmap->height; i++)
	//{
	//	for (int j = 0; j < canvas.device->bitmap->width; j++)
	//	{
	//		cout << (int)canvas.device->bitmap->data[i * width + j].r << "  ";
	//	}
	//	cout << endl;
	//}

	
	encodePng("testout.png", *(canvas.device->bitmap));
}
void drawRect()
{
	//Create and config bitmap
	Bitmap bitmap;
	bitmap.Config(1920, 1080);
	bitmap.AllocPixels();

	//Create device and link with bitmap
	DefaultDevice device;
	device.bitmap = &bitmap;

	//Create canvas and link with device
	Canvas canvas(&device);

	//Set Paint style
	Paint paint;
	paint.setARGB(128, 255, 0, 0);
	paint.line_width = 2;
	paint.style = PaintStyle::STROKE;

	//Create and config rect
	Rect rect;
	rect.pos.x = 300;
	rect.pos.y = 300;
	rect.height = 300;
	rect.width = 500;

	//Draw canvas with paint. Then data in bitmap is drawed.
	canvas.DrawRect(rect, paint);

	//Change color and style of paint
	paint.setARGB(60, 0, 255, 0);
	paint.style = PaintStyle::FILL;

	//Fill the rect
	canvas.DrawRect(rect, paint);

	//Reset rect
	rect.pos.x = 600;
	rect.pos.y = 300;
	rect.height = 300;
	rect.width = 500;

	//Change color and style of paint
	paint.setARGB(60, 0, 255, 255);
	paint.style = PaintStyle::FILL_AND_STROKE;

	//Fill and stroke the rect
	canvas.DrawRect(rect, paint);

	//Output to file testout.png
	encodePng("testout.png", *(canvas.device->bitmap));
}

int main(int argc, char* argv[]) {
	//testAlphaBlendTime();
	drawLine();
	//drawRect();


	return 0;
}
